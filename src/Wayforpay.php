<?php

namespace Drupal\wayforpay;

use Drupal\Core\Config\ConfigFactory;
use Psr\Log\LoggerInterface;
use WayForPay\SDK\Credential\AccountSecretCredential;

/**
 * Wayforpay payment service.
 */
class Wayforpay {

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The smtp logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Credential.
   *
   * @var \WayForPay\SDK\Credential\AccountSecretCredential
   */
  protected $credential;

  /**
   * The class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel.
   */
  public function __construct(ConfigFactory $config_factory, LoggerInterface $logger) {
    $this->config = $config_factory->get('wayforpay.settings');
    $this->logger = $logger;
  }

  /**
   * Gets credential based on the current environment.
   *
   * @return \WayForPay\SDK\Credential\AccountSecretCredential
   *   The credential.
   */
  public function getCredential() {
    if (!isset($this->credential)) {
      if($this->config->get('environment') == 'live') {
        $this->credential = new AccountSecretCredential(
          $this->config->get('merchant_login'),
          $this->config->get('secret_key')
        );
      }
      else {
        // @see https://wiki.wayforpay.com/ru/view/852472
        $this->credential = new AccountSecretCredential(
          'test_merch_n1',
          'flk3409refn54t54t*FNJRET'
        );
      }
    }
    return $this->credential;
  }

}
