<?php

namespace Drupal\wayforpay\Controller;

use Drupal\Core\Config\ConfigFactory;
use Drupal\wayforpay\Event\WayforpayEvents;
use Drupal\wayforpay\Event\WayforpayWebhookEvent;
use Drupal\wayforpay\Wayforpay;
use Psr\Log\LoggerInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WayForPay\SDK\Exception\InvalidFieldException;
use WayForPay\SDK\Handler\ServiceUrlHandler;
use WayForPay\SDK\Response\ServiceResponse;

/**
 * Controller routines for book routes.
 */
class WayforpayWebhookController extends ControllerBase {

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The purchase service.
   *
   * @var \Drupal\wayforpay\Wayforpay
   */
  protected $payment;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The smtp logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Creates a new instance.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\wayforpay\Wayforpay $payment
   *   The purchase service.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel.
   */
  public function __construct(
    EventDispatcherInterface $event_dispatcher,
    Wayforpay                $payment,
    ConfigFactory            $config_factory,
    LoggerInterface          $logger
  ) {
    $this->eventDispatcher = $event_dispatcher;
    $this->payment = $payment;
    $this->config = $config_factory->get('wayforpay.settings');
    $this->logger = $logger;
  }

  /**
   * When this controller is created,
   * it will get the di_example.talk service and store it.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('wayforpay.payment'),
      $container->get('config.factory'),
      $container->get('logger.factory')->get('wayforpay')
    );
  }

  /**
   * Handle webhook.
   */
  public function return(Request $request) {
    try {
      $handler = new ServiceUrlHandler($this->payment->getCredential());
      $response = $handler->parseRequestFromGlobals();
    }
    catch (InvalidFieldException $e) {
      $this->logger->warning($e->getMessage());
      return new Response($this->t('Bad request'), Response::HTTP_BAD_REQUEST);
    }

    // Dispatch the webhook event.
    $this->eventDispatcher
    ->dispatch(WayforpayEvents::RETURN, new WayforpayWebhookEvent($response));

    $this->logger->info('Payment return: <pre>@data</pre>', [
      '@data' => print_r($response, TRUE)
    ]);

    return $this->buildConfirmation($response);
  }

  /**
   * Builds payment confirmation.
   *
   * @param \WayForPay\SDK\Response\ServiceResponse $response
   *   Payment response.
   *
   * @return array
   *   The confirmation render array.
   */
  protected function buildConfirmation(ServiceResponse $response) {
    $email = $response->getTransaction()->getEmail();
    $email_masked = $this->maskEmail($email);
    $status = $response->getTransaction()->getStatus();
    return [
      '#type' => 'wayforpay_confirmation',
      '#order_reference' => $response->getTransaction()->getOrderReference(),
      '#amount' => $response->getTransaction()->getAmount(),
      '#currency' => $response->getTransaction()->getCurrency(),
      '#email' => $email,
      '#email_masked' => $email_masked,
      '#status' => $status,
      '#text' => $this->t('Payment status is @status. Further details were send to your email address @email_masked.', [
        '@status' => $status,
        '@email_masked' => $email_masked,
      ]),
      '#response' => $response,
    ];
  }

  /**
   * Masks email address.
   *
   * @param $email
   *   The email.
   *
   * @return string
   *   Masked email address.
   */
  protected function maskEmail($email, $beginning_length = 2, $ending_length = 2) {
    $email_parts = explode("@",$email);
    $name = implode('@', array_slice($email_parts, 0, count($email_parts) - 1));
    $prefix = substr($name,0, $beginning_length);
    $body = str_repeat('*', strlen($name) - $beginning_length - $ending_length);
    $suffix = substr($name,strlen($name) - $ending_length, $ending_length);
    return $prefix . $body . $suffix . '@' . end($email_parts);
  }

  /**
   * Handle webhook.
   */
  public function service(Request $request) {
    try {
      $handler = new ServiceUrlHandler($this->payment->getCredential());
      $response = $handler->parseRequestFromGlobals();
    }
    catch (InvalidFieldException $e) {
      $this->logger->warning($e->getMessage());
      return new Response($this->t('Bad request'), Response::HTTP_BAD_REQUEST);
    }

    // Dispatch the webhook event.
    $this->eventDispatcher
    ->dispatch(WayforpayEvents::SERVICE, new WayforpayWebhookEvent($response));

    $this->logger->info('Payment service: <pre>@data</pre>', [
      '@data' => print_r($response, TRUE)
    ]);

    return $this->buildConfirmation($response);
  }

}
