<?php

namespace Drupal\wayforpay\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\wayforpay\Event\WayforpayPaymentEvent;
use Drupal\wayforpay\Event\WayforpayEvents;
use Drupal\wayforpay\Wayforpay;
use Psr\Container\ContainerInterface;
use WayForPay\SDK\Collection\ProductCollection;
use WayForPay\SDK\Domain\Client;
use WayForPay\SDK\Domain\Product;
use WayForPay\SDK\Endpoint\PayEndpoint;
use WayForPay\SDK\Wizard\PurchaseWizard;

/**
 * Wayforpay offsite payment redirect form.
 *
 * @package Drupal\wayforpay\Form
 */
class WayforpayOffsitePaymentForm extends FormBase {

  /**
   * The payment service.
   *
   * @var \Drupal\wayforpay\Wayforpay
   */
  protected $payment;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wayforpay_offsite_payment_form';
  }

  /**
   * Class constructor.
   *
   * @param \Drupal\wayforpay\Wayforpay $payment
   *   The payment service.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   */
  public function __construct(Wayforpay $payment, ConfigFactory $config_factory) {
    $this->payment = $payment;
    $this->config = $config_factory->get('wayforpay.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('wayforpay.payment'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['info'] = [
      '#type' => 'markup',
      '#markup' => 'Payment in progress. Please do not close this tab.',
    ];

    if (!empty($this->getRequest()->query)) {
      $form_state->setValues($this->getRequest()->query->all());
    }

    try {
      $this->preparePaymentFields($form, $form_state);

      $form['#attributes']['class'][] = 'wayforpay-payment-redirect-form';
      $form['#attached']['library'][] = 'wayforpay/offsite_redirect';
      $endpoint = new PayEndpoint();
      $form['#action'] = $endpoint->getUrl();
    }
    catch (\Exception $e) {
      $this->messenger()->addError($this->t('Something went wrong'));
    }

    return $form;
  }

  /**
   * Prepares form fields for payment.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function preparePaymentFields(array &$form, FormStateInterface $form_state) {
    $merchant_data = $this->prepareMerchantData($form, $form_state);

    $event = new WayforpayPaymentEvent($form, $form_state, $merchant_data);
    $event_dispatcher = \Drupal::service('event_dispatcher');
    $event_dispatcher->dispatch(WayforpayEvents::PAYMENT, $event);

    foreach ($merchant_data as $key => $value) {
      if (is_array($value)) {
        foreach ($value as $field) {
          $form[$key . '[]'] = [
            '#type' => 'hidden',
            '#value' => $field,
            // Ensure the correct keys by sending values from the form root.
            '#parents' => [$key . '[]'],
          ];
        }
      }
      else {
        $form[$key] = [
          '#type' => 'hidden',
          '#value' => $value,
          // Ensure the correct keys by sending values from the form root.
          '#parents' => [$key],
        ];
      }
    }
  }

  /**
   * Prepares merchant data.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   Merchant data.
   */
  protected function prepareMerchantData(array &$form, FormStateInterface $form_state) {
    $amount = $form_state->getValue('amount');
    $product_name = $form_state->getValue('product_name');
    $product_count = $form_state->getValue('product_count');
    $order_id = $form_state->getValue('order_id');
    $email = $form_state->getValue('email');
    $phone = $form_state->getValue('phone');
    $first_name = strstr($email, '@', TRUE);

    $client = new Client(
      $first_name,
      NULL,
      $email,
      $phone
    );
    $product = new Product($product_name, $amount, $product_count);

    $config = $this->config;

    $return_url = $config->get('return_url') ?? '/wayforpay/return';
    $service_url = $config->get('service_url') ?? '/wayforpay/service';

    $payment_form = PurchaseWizard::get($this->payment->getCredential())
      ->setOrderReference($order_id)
      ->setAmount($amount)
      ->setCurrency($config->get('currency'))
      ->setOrderDate(new \DateTime())
      ->setMerchantDomainName($config->get('merchant_domain'))
      ->setClient($client)
      ->setProducts(new ProductCollection([$product]))
      ->setReturnUrl($this->convertToAbsoluteUrl($return_url))
      ->setServiceUrl($this->convertToAbsoluteUrl($service_url))
      ->getForm();

    $merchant_data = $payment_form->getData();
    return array_filter($merchant_data);
  }

  /**
   * Converts internal URL to an external.
   *
   * @param string $internal_url
   *   Internal URL.
   *
   * @return string
   *   Absolute URL.
   */
  protected function convertToAbsoluteUrl($internal_url) {
    $url = Url::fromUserInput($internal_url);
    $url->setAbsolute(TRUE);
    return $url->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing to do here.
  }

}
