<?php

namespace Drupal\wayforpay\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Wayforpay config form.
 *
 * @package Drupal\wayforpay\Form
 */
class WayforpaySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'wayforpay.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wayforpay_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('wayforpay.settings');

    $form['environment'] = [
      '#type' => 'radios',
      '#title' => $this->t('Environment'),
      '#options' => [
        'test' => $this->t('Test'),
        'live' => $this->t('Live'),
      ],
      '#default_value' => $config->get('environment'),
    ];

    $form['currency'] = [
      '#type' => 'select',
      '#options' => [
        'UAH' => 'UAH',
        'EUR' => 'EUR',
        'USD' => 'USD',
      ],
      '#title' => $this->t('Currency'),
      '#default_value' => $config->get('currency'),
    ];
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#default_value' => $config->get('secret_key'),
      '#maxlength' => 255,
    ];
    $form['merchant_login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant login'),
      '#default_value' => $config->get('merchant_login'),
      '#maxlength' => 255,
    ];
    $form['merchant_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant domain'),
      '#default_value' => $config->get('merchant_domain'),
      '#maxlength' => 255,
    ];
    $form['return_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Return URL'),
      '#default_value' => $config->get('return_url'),
    ];
    $form['service_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service URL'),
      '#default_value' => $config->get('service_url'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->cleanValues();

    $config = $this->config('wayforpay.settings');
    foreach ($form_state->getValues() as $key => $value) {
      if ($key == 'secret_key' && !$value) {
        continue;
      }
      $config->set($key, $value);
    }
    $config->save();
  }

}
