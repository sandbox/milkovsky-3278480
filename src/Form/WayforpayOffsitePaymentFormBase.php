<?php

namespace Drupal\wayforpay\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Wayforpay offsite payment redirect form base.
 *
 * @package Drupal\wayforpay\Form
 */
abstract class WayforpayOffsitePaymentFormBase extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    $form_state->setRedirect('wayforpay.offsite_payment', $form_state->getValues());
  }

}
