<?php

namespace Drupal\wayforpay\Event;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Wraps a wayforpay event for webhook.
 */
class WayforpayPaymentEvent extends Event {

  /**
   * The form.
   *
   * @var array
   */
  private $form;

  /**
   * The form state.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  private $formState;

  /**
   * Data key-value array.
   *
   * @var array
   */
  private $data = [];

  /**
   * Constructor.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   * @param array $data
   *  Data key-value array.
   */
  public function __construct(array &$form, FormStateInterface $formState, array $data) {
    $this->form = &$form;
    $this->formState = $formState;
    $this->data = $data;
  }

  /**
   * Gets the form.
   *
   * @return array
   *   The form.
   */
  public function &getForm(): array {
    return $this->form;
  }

  /**
   * Gets the form state.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   The form state.
   */
  public function getFormState(): FormStateInterface {
    return $this->formState;
  }

  /**
   * Gets the payment data.
   *
   * @return array
   *   The payment data.
   */
  public function getData(): array {
    return $this->data;
  }

}
