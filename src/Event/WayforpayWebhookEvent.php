<?php

namespace Drupal\wayforpay\Event;

use Symfony\Contracts\EventDispatcher\Event;
use WayForPay\SDK\Response\ServiceResponse;

/**
 * Wraps a wayforpay event for webhook.
 */
class WayforpayWebhookEvent extends Event {

  /**
   * Wayforpay API response object.
   *
   * @var \WayForPay\SDK\Response\ServiceResponse
   */
  protected ServiceResponse $response;

  /**
   * Constructs a Wayforpay Webhook Event object.
   *
   * @param \WayForPay\SDK\Response\ServiceResponse $response
   *   Wayforpay API response object.
   */
  public function __construct(ServiceResponse $response) {
    $this->response = $response;
  }

  /**
   * Gets Wayforpay API response object.
   *
   * @return \WayForPay\SDK\Response\ServiceResponse
   *   The Wayforpay API response object
   */
  public function getResponse() :ServiceResponse {
    return $this->response;
  }

}
