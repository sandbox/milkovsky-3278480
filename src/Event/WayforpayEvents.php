<?php

namespace Drupal\wayforpay\Event;

/**
 * Defines events for wayforpay webhooks.
 * */
final class WayforpayEvents {

  /**
   * The name of the event fired when a webhook is received.
   *
   * @Event
   */
  const RETURN = 'wayforpay.return';

  /**
   * The name of the event fired when a webhook is received.
   *
   * @Event
   */
  const SERVICE = 'wayforpay.service';

  /**
   * The name of the event fired when allowing modules to change the client side
   * properties of the wayforpay workflow, like updating prices or massaging
   * post data to fill biling address.
   *
   * @Event
   */
  const PAYMENT = 'wayforpay.payment';

}
