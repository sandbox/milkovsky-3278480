## Offsite payment form implementation

For offsite payment form extend the `\Drupal\wayforpay\Form\WayforpayOffsitePaymentFormBase`.

Example:

```php
use Drupal\wayforpay\Form\WayforpayOffsitePaymentFormBase;

class MyForm extends WayforpayOffsitePaymentFormBase {

  ...

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('order_id', '1');
    $form_state->setValue('product_name', 'donate');
    $form_state->setValue('product_count', 1);
    $form_state->setValue('phone', '+436600000000');
    parent::submitForm($form, $form_state);
  }

}
```
