/**
 * @file
 * Defines behaviors for the payment redirect form.
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Attaches the payment redirect behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the payment redirect behavior.
   */
  Drupal.behaviors.wayforpayPaymentRedirect = {
    attach: function (context) {
      $('.wayforpay-payment-redirect-form', context).submit();
    }
  };

})(jQuery, Drupal, drupalSettings);
